// ECFinderConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

bool test;
bool hasClasses;
std::vector<int> filteredColumns;

void executeAlgorithm(std::ifstream &input, char* argv[]) {
	int minFrequency = std::stoi(argv[2]);
	IOActions ioActions(test, hasClasses, filteredColumns);
	clock_t startTime = clock();
	std::vector<int> table;
	ioActions.parseInputTable(input, table);
	std::cout << "table size: " << std::endl;
	int numberOfRows = table.size() / (ioActions.colLength.size() - 1);
	//minFrequency = numberOfRows * minFrequency / 100;
	std::cout << "\t rows=" << numberOfRows << std::endl;
	std::cout << "\t cols=" << ioActions.colLength.size() - 1 << std::endl;

	std::cout << "Time taken to read data: " << clock() - startTime << " milliseconds" << std::endl;

	startTime = clock();
	ECFinder finder(table, ioActions.colLength, minFrequency);
	finder.findAllEquivalenceClasses();
	std::cout << "Time taken to find equivalence classes: " << clock() - startTime << " milliseconds" << std::endl;

	startTime = clock();
	ioActions.writeResultToOutput(argv[3], finder.resultMap);
	std::cout << "Time taken to write equivalence class to output: " << clock() - startTime << " milliseconds" << std::endl;

	std::vector<EquivalenceClassPointer>  result;
	for (const auto &kv : finder.resultMap)
	{
		result.push_back(kv.second);
	}

	startTime = clock();
	ClusterFinder clusterFinder(ioActions.colLength.size() - 1, minFrequency, table);
	clusterFinder.findClusters(result);
	std::cout << "Time taken to find clusters: " << clock() - startTime << " milliseconds" << std::endl;

	startTime = clock();
	ioActions.writeClustersToOutput(argv[3], clusterFinder.result, clusterFinder.availableRows, input);
	std::cout << "Time taken to write clusters to output: " << clock() - startTime << " milliseconds" << std::endl;
}

void readFilteredColumns(char* filter){
	std::string segment;
	int startcolumn;
	int endcolumn;
	std::stringstream paramStream(filter);
	std::stringstream segmentStream;
	std::string column;

	for (size_t col = 0; std::getline(paramStream, segment, ','); col++)
	{
		std::stringstream segmentStream(segment);
		std::getline(segmentStream, column, '-');
		startcolumn = std::stoi(column);
		filteredColumns.push_back(startcolumn - 1);
		if (std::getline(segmentStream, column)) {
			endcolumn = std::stoi(column);
			for (startcolumn++; startcolumn <= endcolumn; startcolumn++){
				filteredColumns.push_back(startcolumn - 1);
			}
		}
	}
	std::sort(filteredColumns.begin(), filteredColumns.end());
}

int main(int argc, char* argv[])
{
	if (argc > 4) {
		test = std::strcmp(argv[4], "t") == 0;
		hasClasses = std::strcmp(argv[4], "c") == 0;
		if (!test && !hasClasses) {
			test = hasClasses = std::strcmp(argv[4], "ct") == 0;
		}
		if (!test && !hasClasses || argc > 5) {
			readFilteredColumns(!test && !hasClasses ? argv[4] : argv[5]);
		}
	}
	if (argc < 4 || argc > 6) {
		printf("Usage\n");
		printf("\t%s  data_filename  min_frequency output_filename\n", argv[0]);
		printf("[optional]:\n\t c - if the source files first column contains class labels. \n");
		printf("\t Filtering. E.g 1-3,5,18. Must contain no spaces. The result does not use the original column numbers.\n");
		return 0;
	}
	printf("Source file: %s \n", argv[1]);
	printf("Minimal frequency: %s \n", argv[2]);
	printf("Output file prefix: %s \n", argv[3]);
	printf("Does source file contain classes: %s \n", hasClasses ? "true" : "false");
	printf("Columns used: %s \n", filteredColumns.size() > 0 ? argv[argc - 1] : "all");
	std::ifstream input(argv[1]);
	if (input) {
		try
		{
			executeAlgorithm(input, argv);
		}
		catch (std::exception& e)
		{
			std::cout << "Exception: " << e.what() << std::endl;
		}
	}
	else {
		printf("File with name [%s] not found", argv[1]);
	}
	input.close();

	_CrtDumpMemoryLeaks();

	return 0;
}

