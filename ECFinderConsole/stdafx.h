// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

/* Used for memory leak detection*/
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <iostream>
#include <fstream>
#include <sstream> /* for stringstream*/
#include <string>
#include <vector>
#include <ctime>
#include <iomanip>
#include <unordered_map>
#include <algorithm>

#include "IOActions.h"

#include "ECFinder.h"
#include "ClusterFinder.h"

// TODO: reference additional headers your program requires here
