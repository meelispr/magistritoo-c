#pragma once
#include <unordered_map>
#include "ECFinder.h"

struct TranslationKey
{
	size_t col;
	int value;

	TranslationKey(size_t col, int value) :col(col), value(value){}
	bool operator==(const TranslationKey &other) const
	{
		return col == other.col && value == other.value;
	}
};

struct TranslationKeyHasher
{
	size_t operator()(const TranslationKey& key) const
	{
		return (std::hash<size_t>()(key.col) ^ (std::hash<int>()(key.value)));
	}
};

struct ColValPair{
	int col;
	std::string val;

	ColValPair(int col, std::string val) :col(col), val(val){}
};
class IOActions
{
public:
	IOActions(bool test, bool hasClasses, std::vector<int> &filteredColumns);
	~IOActions();
	void IOActions::parseInputTable(std::ifstream &input, std::vector<int> &table);
	void IOActions::writeResultToOutput(char* &outputFilePrefix, std::unordered_map<EquivalenceClassKey, EquivalenceClassPointer, EquivalenceClassKeyHasher> &resultMap);
	void IOActions::writeClustersToOutput(char* &outputFilePrefix, const std::vector<EquivalenceClassPointer> &clusters, std::vector<int> &rows, std::ifstream &input);

	std::vector<size_t> colLength;
private:
	bool test;
	bool hasClasses;
	std::vector<int> filteredColumns;
	std::vector<int> classValues;
	std::vector<ColValPair> difference;
	int classLabelCounter = 0;
	std::unordered_map<TranslationKey, int, TranslationKeyHasher> conversionTable;
	std::unordered_map<TranslationKey, std::string, TranslationKeyHasher> reverseConversionTable;

	inline int IOActions::writeClusterToOutput(std::ofstream &clusterOutput, EquivalenceClassPointer &cluster, std::vector<int> &classDistribution, int clusterIdx);
	inline void IOActions::writeRowClusterDistributionToOutput(char* &outputFilePrefix, std::ifstream &input);
	inline void IOActions::writeClosedSetToOutput(std::ofstream &closedOutput, EquivalenceClassPointer &equivalenceClass);
	inline int IOActions::writeGeneratorAndPairToOutput(EquivalenceClassPointer &equivalenceClass, std::ofstream &generatorOutput, std::ofstream &pairOutput, std::ofstream &fullPairOutput, int generatorCount, int closedSetCount);
	inline std::string IOActions::decode(size_t col, int encodedValue);
	inline int IOActions::encode(size_t col, std::string originalValue);
	int IOActions::writeTrashClusterToOutput(std::ofstream &clusterOutput, const std::vector<EquivalenceClassPointer> & clusters, std::vector<int> &rows, std::vector<int> &classDistribution, int clusterIdx);
	inline void IOActions::updateColLength();
};

