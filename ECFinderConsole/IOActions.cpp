#include "stdafx.h"
#include "IOActions.h"

IOActions::IOActions(bool test, bool hasClasses, std::vector<int> &filteredColumns) : test(test), hasClasses(hasClasses), filteredColumns(filteredColumns)
{
}


IOActions::~IOActions()
{
}


void IOActions::parseInputTable(std::ifstream &input, std::vector<int> &table)
{
	std::string line;
	std::string originalValue;
	bool first = true;
	std::unordered_map<std::string, int> classConversionTable;
	while (input.good())
	{
		std::getline(input, line);
		if (line.size() > 1) {
			if (first) {
				size_t count;
				if (filteredColumns.size() > 0) {
					count = filteredColumns.size();
				}
				else { 
					count = std::count(line.begin(), line.end(), ' ');
					if (!hasClasses) {
						count++;
					}
				}
				colLength.resize(count + 1);
				conversionTable.reserve(count * 4);
				reverseConversionTable.reserve(count * 4);
				first = false;
			}
			std::stringstream lineStream(line);
			if (hasClasses) {
				std::getline(lineStream, originalValue, ' '); /* Since label is not used*/

				int classValue = classConversionTable[originalValue];
				if (classValue == 0) {
					classValue = ++classLabelCounter;
					classConversionTable[originalValue] = classValue;
				}
				classValues.push_back(classValue);
			}
			else {
				classValues.push_back(1);
			}

			if (filteredColumns.size() > 0) {
				auto it = filteredColumns.begin();
				for (size_t realCol = 0, filteredCol = 0; std::getline(lineStream, originalValue, ' ') && it < filteredColumns.end(); realCol++)
				{
					if (*it == realCol) {
						it++;
						table.push_back(encode(filteredCol++, originalValue));
					}
				}
			}
			else {
				for (size_t col = 0; std::getline(lineStream, originalValue, ' '); col++)
				{
					table.push_back(encode(col, originalValue));
				}
			}
		}
	}

	if (hasClasses) {
		std::cout << "In the result cluster file the class position of the value  :" << std::endl;
		for (auto &kv : classConversionTable) {
			std::cout << "\t" << kv.first << ". = " << kv.second << std::endl;
		}
		std::cout << "Number of classes:" << classLabelCounter << std::endl;
	}
	updateColLength();
}

void IOActions::writeResultToOutput(char* &outputFilePrefix, std::unordered_map<EquivalenceClassKey, EquivalenceClassPointer, EquivalenceClassKeyHasher> &resultMap)
{
	std::ofstream closedOutput(outputFilePrefix + std::string(".closed"));
	std::ofstream generatorOutput(outputFilePrefix + std::string(".key"));
	std::ofstream pairOutput(outputFilePrefix + std::string(".pair"));
	std::ofstream fullPairOutput(outputFilePrefix + std::string(".fullpair"));


	int closedSetCount = 0, generatorCount = 0;
	for (auto &kv : resultMap)
	{
		if (test) {
			closedSetCount++;
			generatorCount += kv.second->generators.size() / kv.second->closedSet.size();
			continue;
		}
		writeClosedSetToOutput(closedOutput, kv.second);
		closedOutput << "    " << kv.second->frequency << std::endl;
		generatorCount = writeGeneratorAndPairToOutput(kv.second, generatorOutput, pairOutput, fullPairOutput, generatorCount, closedSetCount);
		closedSetCount++;
	}
	std::cout << "Number of closed sets: " << closedSetCount << std::endl;
	std::cout << "Number of generators: " << generatorCount << std::endl;
	closedOutput.close();
	generatorOutput.close();
	pairOutput.close();
	fullPairOutput.close();
}

void IOActions::writeClustersToOutput(char* &outputFilePrefix, const std::vector<EquivalenceClassPointer> &clusters, std::vector<int> &rows, std::ifstream &input)
{
	std::ofstream clusterOutput(outputFilePrefix + std::string(".clusters")); 
	std::vector<int> classDistribution(classLabelCounter);
	int errors = 0;
	int clusterIdx = 0;
	for (EquivalenceClassPointer cluster : clusters)
	{
		if (test) {
			break;
		}
		errors += writeClusterToOutput(clusterOutput, cluster, classDistribution, clusterIdx);
		clusterIdx++;
	}
	std::cout << "Number of clusters without trash cluster: " << clusters.size() << std::endl;
	if (hasClasses) {
		errors += writeTrashClusterToOutput(clusterOutput, clusters, rows, classDistribution, clusterIdx);
		std::cout << "Number of errors in clusters: " << errors << std::endl;
	}
	writeRowClusterDistributionToOutput(outputFilePrefix, input);
	clusterOutput.close();
}
inline void IOActions::writeRowClusterDistributionToOutput(char* &outputFilePrefix, std::ifstream &input){
	/*current date+time*/
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::stringstream buffer;
	buffer << std::put_time(&tm, "-%d-%m-%Y-%H-%M-%S");

	std::ofstream rowClusterOutput(outputFilePrefix + buffer.str() + std::string(".rowClusters"));
	int rowIdx = 0;
	input.clear();
	input.seekg(0, std::ios::beg);
	std::string line;
	for (int clusterDistribution : classValues) {
		std::getline(input, line);
		rowClusterOutput << line << ' ' << clusterDistribution << std::endl;
	}
	rowClusterOutput.close();
}
inline int IOActions::writeClusterToOutput(std::ofstream &clusterOutput, EquivalenceClassPointer &cluster, std::vector<int> &classDistribution, int clusterIdx)
{
	writeClosedSetToOutput(clusterOutput, cluster);
	clusterOutput << "    " << cluster->rows.size();
	if (!hasClasses) {
		clusterOutput << std::endl;
		for (int rowIdx : cluster->rows) {
			classValues[rowIdx] = clusterIdx; //reuse the classValue array for cluster distribution
		}
		return 0;
	}

	for (int rowIdx : cluster->rows) {
		classDistribution[classValues[rowIdx] - 1]++;
		classValues[rowIdx] = clusterIdx; //reuse the classValue array for cluster distribution
	}
	int max = 0;
	for (int distribution : classDistribution) {
		if (distribution > max) {
			max = distribution;
		}
		clusterOutput << " " << distribution;
	}
	clusterOutput << std::endl;
	int errors = 0;
	for (int distribution : classDistribution) {
		if (distribution > 0 && distribution != max) {
			errors += distribution;
		}
	}
	std::fill(classDistribution.begin(), classDistribution.end(), 0);
	return errors;
}

inline void IOActions::writeClosedSetToOutput(std::ofstream &closedOutput, EquivalenceClassPointer &equivalenceClass)
{
	if (test) {
		return;
	}
	closedOutput << equivalenceClass->closedSet.size();
	for (size_t col = 0; col < equivalenceClass->closedSet.size(); col++)
	{
		closedOutput << ' ' << decode(col, equivalenceClass->closedSet[col]);
	}
}

inline int IOActions::writeGeneratorAndPairToOutput(EquivalenceClassPointer &equivalenceClass, std::ofstream &generatorOutput, std::ofstream &pairOutput, std::ofstream &fulPairOutput, int generatorCount, int closedSetCount)
{
	if (test) {
		return generatorCount + equivalenceClass->generators.size() / equivalenceClass->closedSet.size();
	}

	std::string value;
	difference.reserve(equivalenceClass->closedSet.size());
	for (size_t count = 0; count < equivalenceClass->generators.size(); count++)
	{
		int col = count % equivalenceClass->closedSet.size();
		if (col == 0) {
			if (difference.size() > 0) {
				fulPairOutput << " ZeroF";
				for (ColValPair pair : difference)
				{
					fulPairOutput << " V" << pair.col << '.' << pair.val;
				}
				difference.clear();
			}
			if (generatorCount > 0){
				fulPairOutput << std::endl;
			}
			fulPairOutput << equivalenceClass->frequency << " G" << generatorCount;
			generatorOutput << equivalenceClass->closedSet.size();
			pairOutput << generatorCount++ << ' ' << closedSetCount << std::endl;
		}
		value = decode(col, equivalenceClass->generators[count]);
		if (equivalenceClass->generators[count] != 0) {
			fulPairOutput << " V" << col + 1 << '.' << value;
		}
		else if (equivalenceClass->closedSet[col] != 0){
			difference.emplace_back(ColValPair{ col + 1, decode(col, equivalenceClass->closedSet[col]) });
		}
		generatorOutput << ' ' << value;
		if ((count + 1) % equivalenceClass->closedSet.size() == 0){
			generatorOutput << std::endl;
		}
	}

	return generatorCount;
}

inline int IOActions::encode(size_t col, std::string originalValue)
{
	TranslationKey key = { col, std::stoi(originalValue) };
	int kv = conversionTable[key];
	if (kv == 0) {
		int encodedValue = ++colLength[col + 1];
		conversionTable[key] = encodedValue;
		reverseConversionTable[{col, encodedValue}] = originalValue;
		return encodedValue;
	}
	return kv;
}

inline std::string IOActions::decode(size_t col, int encodedValue)
{
	if (encodedValue == 0) {
		return "-";
	}
	return reverseConversionTable[{col, encodedValue}];
}

inline void IOActions::updateColLength()
{
	for (size_t i = 0; i < (colLength.size() - 1); i++)
	{
		colLength[i + 1] += colLength[i];
	}
}

int IOActions::writeTrashClusterToOutput(std::ofstream &clusterOutput, const std::vector<EquivalenceClassPointer> & clusters, std::vector<int> &rows, std::vector<int> &classDistribution, int clusterIdx)
{
	EquivalenceClassPointer errorCluster = clusters[0];
	std::fill(errorCluster->closedSet.begin(), errorCluster->closedSet.end(), 0);
	errorCluster->rows.swap(rows);

	return writeClusterToOutput(clusterOutput, errorCluster, classDistribution, clusterIdx);
}
