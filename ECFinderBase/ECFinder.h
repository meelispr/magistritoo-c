#pragma once
#include <vector>
#include <unordered_map>
#include <memory>

struct FrequencyValue
{
	int frequency;
	int row;
	int col;

	int getOriginalTableCol() { return row; }
	int getOriginalTableColValue() { return col + 1; }

	FrequencyValue(int frequency, int row, int col) :frequency(frequency), row(row), col(col){}
};
struct EquivalenceClass
{
	int frequency;
	std::vector<int> closedSet;
	std::vector<int> generators;

	double numberOfFilledColumnsInCS;
	double qualityIndex;
	double discriminativenessIndex;
	std::vector<int> rows;

	EquivalenceClass(int frequency, const std::vector<int> &closedSet, std::vector<int> generators)
		:frequency(frequency), closedSet(closedSet), generators(generators){}
};
typedef std::shared_ptr<EquivalenceClass> EquivalenceClassPointer;

struct EquivalenceClassKey
{
	int frequency;
	std::vector<int> closedSet;

	EquivalenceClassKey(int frequency, const std::vector<int> &closedSet) :frequency(frequency), closedSet(closedSet){}

	bool operator==(const EquivalenceClassKey &other) const
	{
		return frequency == other.frequency && closedSet == other.closedSet;
	}
};
struct EquivalenceClassKeyHasher
{
	static std::vector<size_t> colLengthHashed;
	size_t operator()(const EquivalenceClassKey& key) const
	{
		size_t hash = 0;
		for (size_t col = 0; col < key.closedSet.size(); col++)
		{
			hash += key.closedSet[col] * colLengthHashed[col];
		}
		return hash;
	}
};

class ECFinder
{
public:
	ECFinder(const std::vector<int> &table, const std::vector<size_t>&colLength, int allowedMinFreq);
	~ECFinder();
	inline void initializeFrequencytable(std::vector<int > &frequencyTable);
	inline void initializeSubMatrix(std::vector<int > &subMatrix);
	inline void orderFrequencyTableInAscendingOrderAndZeroesDown(std::vector< int> &freqTable, const std::vector< int> &oldFreqTable, int maxFrequency, std::vector<FrequencyValue> &orderedFrequencies, const std::vector< int> &closedSet);
	inline void populateNewSubMatrixAndFrequencyTable(const std::vector< int > &oldSubMatrix, FrequencyValue &maxFrequency, std::vector< int > &newSubMatrix, std::vector< int > &newFrequencyTable);
	inline bool populateClosedSetAndReturnIfExisting(const std::vector< int> &oldFrequencyTable, const std::vector< int> &newFrequencyTable, int maxFrequency, std::vector<int> &closedSet);
	inline bool addToResultAndReturnIfNew(const std::vector< int> &oldFrequencyTable, const std::vector< int> &newFrequencyTable, int maxFrequency, std::vector<int> &closedSet);
	inline void findEquivalenceClass(const std::vector< int > &oldSubmatrix, const std::vector<int> &oldFrequencyTable, FrequencyValue &maxFrequency, const std::vector<int> &oldClosedSet);
	void findAllEquivalenceClasses();
	void findFrequencyClasses(const std::vector<int> &submatrix, const std::vector< int > &oldFrequencyTable, std::vector< int > &newFrequencyTable, int maxFrequency, const std::vector< int > &closedSet);
	std::vector<int> currentGenerator;
	std::unordered_map<EquivalenceClassKey, EquivalenceClassPointer, EquivalenceClassKeyHasher> resultMap;
private:
	std::vector<int> table;
	std::vector<size_t> colLength;
	size_t numberOfColumns;
	int allowedMinFreq;
};

