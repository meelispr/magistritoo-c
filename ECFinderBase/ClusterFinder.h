#pragma once
#include "ECFinder.h"

class ClusterFinder
{
public:
	ClusterFinder(size_t numberOfColumns, int minFrequency, std::vector<int> table);
	~ClusterFinder();
	inline void findDiscriminativenessIndex(EquivalenceClassPointer &candidateCluster);
	inline double findHomogeneityIndex(EquivalenceClassPointer &candidateCluster);
	inline void findQualityIndex(EquivalenceClassPointer &candidateCluster);
	EquivalenceClassPointer findFinalCluster(std::vector<EquivalenceClassPointer> &candidateClusters);
	inline void removeExtractedRowsAndReduceFrequencies(EquivalenceClassPointer &finalCluster, std::vector<EquivalenceClassPointer> &candidateClusters);
	inline EquivalenceClassPointer findNextFinalCluster(std::vector<EquivalenceClassPointer> &candidateClusters);
	void findClusters(std::vector<EquivalenceClassPointer> &candidateClusters);
	std::vector<int> table;
	std::vector<int> availableRows;
	std::vector<int> tempAvailableRows;
	std::vector<int> potentialAvailableRows;
	std::vector<int> tempUsedRows;
	std::vector<int> potentialUsedRows;
	std::vector<EquivalenceClassPointer> result;
private:
	inline int calculateFrequency(const std::vector<int> &closedSet);
	size_t numberOfColumns;
	int minFrequency;
};

