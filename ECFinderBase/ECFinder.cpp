#include "stdafx.h"
#include "ECFinder.h"

std::vector<size_t> EquivalenceClassKeyHasher::colLengthHashed;

ECFinder::ECFinder(const std::vector<int> &table, const std::vector<size_t>&colLength, int allowedMinFreq) 
:table(table), colLength(colLength), resultMap(200000), currentGenerator((colLength.size() - 1)),
allowedMinFreq(allowedMinFreq)
{
	numberOfColumns = (colLength.size() - 1);
	std::vector<size_t> colLengthHashed(numberOfColumns);
	colLengthHashed[0] = 1;
	for (size_t col = 1; col < colLengthHashed.size(); col++)
	{
		colLengthHashed[col] = (colLength[col] - colLength[col - 1]) * colLengthHashed[col - 1];
	}
	EquivalenceClassKeyHasher::colLengthHashed = colLengthHashed;
}

ECFinder::~ECFinder(){}

void ECFinder::findAllEquivalenceClasses()
{
	std::vector< int > frequencyTable(colLength.back());
	std::vector< int > closedSet(numberOfColumns);
	std::vector<int > subMatrix(table.size() / numberOfColumns);
	initializeFrequencytable(frequencyTable);
	initializeSubMatrix(subMatrix);
	findFrequencyClasses(subMatrix, frequencyTable, frequencyTable, std::numeric_limits<int>::max(), closedSet);
}

void ECFinder::findFrequencyClasses(const std::vector<int> &submatrix, const std::vector< int > &oldFrequencyTable, std::vector< int > &newFrequencyTable, int maxFrequency, const std::vector< int > &closedSet)
{
	std::vector< FrequencyValue > orderedFrequencies;
	orderFrequencyTableInAscendingOrderAndZeroesDown(newFrequencyTable, oldFrequencyTable, maxFrequency, orderedFrequencies, closedSet);
	for (auto & minFrequency : orderedFrequencies)
	{
		currentGenerator[minFrequency.getOriginalTableCol()] = minFrequency.getOriginalTableColValue();
		newFrequencyTable[colLength[minFrequency.row] + minFrequency.col] = 0;
		findEquivalenceClass(submatrix, newFrequencyTable, minFrequency, closedSet);
		currentGenerator[minFrequency.getOriginalTableCol()] = 0;
	}
}

inline bool sortByFrequency(const FrequencyValue &leftFreq, const FrequencyValue &rightFrequency)
{
	if (leftFreq.frequency == rightFrequency.frequency) {
		if (leftFreq.row == rightFrequency.row) {
			return leftFreq.col < rightFrequency.col;
		}
		return leftFreq.row < rightFrequency.row;
	}
	return leftFreq.frequency < rightFrequency.frequency;
}

inline void ECFinder::orderFrequencyTableInAscendingOrderAndZeroesDown(std::vector< int> &freqTable, const std::vector< int> &oldFreqTable, int maxFrequency, std::vector<FrequencyValue> &orderedFrequencies, const std::vector< int> &closedSet)
{
	orderedFrequencies.reserve(freqTable.size());
	auto freqIt = freqTable.begin();
	auto oldfreqIt = oldFreqTable.begin();
	auto colIt = colLength.begin();
	auto closedSetIt = closedSet.begin();
	for (size_t row = 0; colIt < colLength.end() - 1; row++, colIt++, closedSetIt++){
		if (*closedSetIt == 0) {
			for (size_t col = 0; freqIt < freqTable.begin() + *(colIt + 1); col++, freqIt++, oldfreqIt++)
			{
				if (*oldfreqIt == 0) {
					*freqIt = 0;
					continue;
				}
				if (*freqIt >= allowedMinFreq && *freqIt < maxFrequency){
					orderedFrequencies.emplace_back(*freqIt, row, col);
				}
			}
		}
		else {
			freqIt += *(colIt + 1) - *colIt;
			oldfreqIt += *(colIt + 1) - *colIt;
		}
	}
	std::sort(orderedFrequencies.begin(), orderedFrequencies.end(), sortByFrequency);
}

inline void ECFinder::findEquivalenceClass(const std::vector< int > &oldSubmatrix, const std::vector<int> &oldFrequencyTable, FrequencyValue &maxFrequency, const std::vector<int> &oldClosedSet)
{
	std::vector< int > newSubmatrix(maxFrequency.frequency);
	std::vector< int > newFrequencyTable(oldFrequencyTable.size());
	std::vector< int > newClosedSet(oldClosedSet);
	newClosedSet[maxFrequency.getOriginalTableCol()] = maxFrequency.getOriginalTableColValue();
	populateNewSubMatrixAndFrequencyTable(oldSubmatrix, maxFrequency, newSubmatrix, newFrequencyTable);
	addToResultAndReturnIfNew(oldFrequencyTable, newFrequencyTable, maxFrequency.frequency, newClosedSet);
	if (maxFrequency.frequency > allowedMinFreq) {
		findFrequencyClasses(newSubmatrix, oldFrequencyTable, newFrequencyTable, maxFrequency.frequency, newClosedSet);
	}
}

inline void ECFinder::populateNewSubMatrixAndFrequencyTable(const std::vector< int > &oldSubMatrix, FrequencyValue &maxFrequency, std::vector< int > &newSubMatrix, std::vector< int > &newFrequencyTable)
{
	auto it = newSubMatrix.begin();
	auto tableIt = table.begin();
	for (const int rowIdx : oldSubMatrix)
	{
		tableIt = table.begin() + rowIdx * numberOfColumns;
		if ( *(tableIt + maxFrequency.getOriginalTableCol()) == maxFrequency.getOriginalTableColValue()) {
			*it = rowIdx;
			++it;
			for (auto colIt = colLength.begin(); colIt < colLength.end() - 1; colIt++, tableIt++)
			{
				newFrequencyTable[*colIt + *tableIt - 1]++;
			}
		}
	}
}

inline bool ECFinder::addToResultAndReturnIfNew(const std::vector< int> &oldFrequencyTable, const std::vector< int> &newFrequencyTable,
	int maxFrequency, std::vector<int> &closedSet )
{
	bool isExisting = populateClosedSetAndReturnIfExisting(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet);
	if (isExisting) {
		EquivalenceClassPointer equivalenceClass = resultMap[{maxFrequency, closedSet}];
		equivalenceClass->generators.reserve(equivalenceClass->generators.size() + currentGenerator.size());
		equivalenceClass->generators.insert(equivalenceClass->generators.end(), currentGenerator.begin(), currentGenerator.end());
	}
	else 
	{
		EquivalenceClassPointer equivalenceClass(new EquivalenceClass(maxFrequency, closedSet, currentGenerator));
		resultMap[{maxFrequency, closedSet}] = equivalenceClass;
	}
	return !isExisting;
}

inline bool ECFinder::populateClosedSetAndReturnIfExisting(const std::vector< int> &oldFrequencyTable, const std::vector< int> &newFrequencyTable, int maxFrequency, std::vector<int> &closedSet)
{
	int savedSize = 0;
	bool isExisting = false;
	auto freqIt = newFrequencyTable.begin();
	auto oldfreqIt = oldFrequencyTable.begin();
	auto colIt = colLength.begin();
	auto closedSetIt = closedSet.begin();
	for (size_t realCol = 0; colIt < colLength.end() - 1; colIt++, realCol++, closedSetIt++)
	{
		if (*closedSetIt == 0)
		{
			for (size_t col = 0; freqIt < newFrequencyTable.begin() + *(colIt + 1); col++, freqIt++, oldfreqIt++)
			{
				if (*freqIt == maxFrequency) {
					*closedSetIt = col + 1;
					if (*oldfreqIt == 0) {
						isExisting = true;
					}
					freqIt = newFrequencyTable.begin() + *(colIt + 1);
					oldfreqIt = oldFrequencyTable.begin() + *(colIt + 1);
					break;
				}
			}
		}
		else {
			freqIt += *(colIt + 1) - *colIt;
			oldfreqIt += *(colIt + 1) - *colIt;
		}
	}
	return isExisting;
}

inline void ECFinder::initializeFrequencytable(std::vector<int > &frequencyTable)
{
	for (size_t count = 0; count < table.size(); count++)
	{
		frequencyTable[colLength[count % numberOfColumns] + (table[count] - 1)]++;
	}
}

inline void ECFinder::initializeSubMatrix(std::vector<int > &subMatrix)
{
	for (size_t count = 0; count < table.size() / numberOfColumns; count++)
	{
		subMatrix[count] = count;
	}
}
