#include "stdafx.h"
#include "ClusterFinder.h"


ClusterFinder::ClusterFinder(size_t numberOfColumns, int minFrequency, std::vector<int> table)
: numberOfColumns(numberOfColumns), minFrequency(minFrequency), table(table), availableRows(table.size() / numberOfColumns, 1)
{
	for (size_t count = 0; count < availableRows.size(); count++)
	{
		availableRows[count] = count;
	}
	tempAvailableRows.reserve(availableRows.size());
	potentialAvailableRows.reserve(availableRows.size());
	tempUsedRows.reserve(availableRows.size());
	potentialUsedRows.reserve(availableRows.size());
}
ClusterFinder::~ClusterFinder()
{
}
std::vector<EquivalenceClassPointer> tmpCandidateClusters;

inline void ClusterFinder::findDiscriminativenessIndex(EquivalenceClassPointer &candidateCluster)
{
	int genFilledSize = 0;
	candidateCluster->numberOfFilledColumnsInCS = 0;
	candidateCluster->discriminativenessIndex = 1;
	for(const int value : candidateCluster->closedSet) {
		if (value != 0) {
			candidateCluster->numberOfFilledColumnsInCS++;
		}
	}
	for (size_t i = 0; i < candidateCluster->generators.size(); i++)
	{
		if (candidateCluster->generators[i] != 0)
		{
			genFilledSize++;
		}
		if ((i + 1) % numberOfColumns == 0)
		{
			candidateCluster->discriminativenessIndex *= (1 + (candidateCluster->numberOfFilledColumnsInCS - genFilledSize) / candidateCluster->numberOfFilledColumnsInCS);
			genFilledSize = 0;
		}
	}
}

inline double ClusterFinder::findHomogeneityIndex(EquivalenceClassPointer &candidateCluster)
{
	return candidateCluster->numberOfFilledColumnsInCS / numberOfColumns;
}

inline void ClusterFinder::findQualityIndex(EquivalenceClassPointer &candidateCluster)
{
	findDiscriminativenessIndex(candidateCluster);
	candidateCluster->qualityIndex = candidateCluster->discriminativenessIndex *
		findHomogeneityIndex(candidateCluster);
}

inline bool sortByQualityIndex(const EquivalenceClassPointer &leftFreq, const EquivalenceClassPointer &rightFrequency)
{
	if (leftFreq->qualityIndex == rightFrequency->qualityIndex) {
		return leftFreq->frequency > rightFrequency->frequency;
	}
	return leftFreq->qualityIndex > rightFrequency->qualityIndex;
}

void ClusterFinder::findClusters(std::vector<EquivalenceClassPointer> &candidateClusters)
{
	/*Find QIs*/
	for (auto & candidateCluster : candidateClusters)
	{
		findQualityIndex(candidateCluster);
	}
	/*sort*/
	std::sort(candidateClusters.begin(), candidateClusters.end(), sortByQualityIndex);
	/*find all clusters*/
	int frequency;
	int maxFrequency;
	EquivalenceClassPointer finalCluster;
	unsigned int unsignedMinFrequency = (unsigned int) minFrequency;
	for (auto cluster = candidateClusters.begin(); cluster < candidateClusters.end() && availableRows.size() >= unsignedMinFrequency; cluster++)
	{
		if ((*cluster)->frequency < minFrequency)
		{
			continue;
		}

		/* calculate new freq*/
		maxFrequency = calculateFrequency((*cluster)->closedSet);
		if (maxFrequency < minFrequency)
		{
			continue;
		}
		(*cluster)->frequency = maxFrequency;
		finalCluster = (*cluster);
		potentialAvailableRows.swap(tempAvailableRows);
		potentialUsedRows.swap(tempUsedRows);

		/*Check for same QI elements*/
		for (auto tempCluster = cluster + 1; tempCluster < candidateClusters.end() && !((*tempCluster)->qualityIndex < (*cluster)->qualityIndex); tempCluster++)
		{
			frequency = calculateFrequency((*tempCluster)->closedSet);
			(*tempCluster)->frequency = frequency;
			if (frequency > maxFrequency) {
				finalCluster = (*tempCluster);
				maxFrequency = frequency;
				potentialAvailableRows.swap(tempAvailableRows);
				potentialUsedRows.swap(tempUsedRows);
			}
		}
		if (finalCluster != (*cluster)) {
			cluster--; /* this step is necessary in order to recheck the current cluster when it is not the final one*/
		}

		/*remove used rows*/
		availableRows.swap(potentialAvailableRows);
		
 		finalCluster->rows = potentialUsedRows;
		result.push_back(finalCluster);
	}
}

inline int ClusterFinder::calculateFrequency(const std::vector<int> &closedSet){
	int frequency = 0;
	auto tableIt = table.begin();
	bool containedInRow;
	tempAvailableRows.clear();
	tempUsedRows.clear();
	for (const int rowIdx : availableRows){
		tableIt = table.begin() + rowIdx * numberOfColumns;
		containedInRow = true;
		for (auto closedSetIt = closedSet.begin(); closedSetIt < closedSet.end(); closedSetIt++, tableIt++) {
			if (*closedSetIt != 0 && *closedSetIt != *tableIt) {
				containedInRow = false;
				break;
			}
		}
		if (containedInRow) {
			frequency++;
			tempUsedRows.emplace_back(rowIdx);
		}
		else {
			tempAvailableRows.emplace_back(rowIdx);
		}
	}

	return frequency;
}
