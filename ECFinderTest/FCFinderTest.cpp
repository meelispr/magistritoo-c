#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ECFinderTest
{		
	TEST_CLASS(ECFinderTest)
	{
	public:
		TEST_METHOD(initializeFrequencytable)
		{
			ECFinder finder = initECFinder();
			std::vector< int> expected = {
				3, 0, 2,
				1, 2, 2,
				4, 1,
				3, 2 };
			std::vector< int > frequencyTable(expected.size());
			finder.initializeFrequencytable(frequencyTable);
			compare(expected, frequencyTable);
		}
		
		TEST_METHOD(orderFrequencyValues)
		{
			ECFinder finder = initECFinder();
			std::vector< int > frequencyTable(10);
			std::vector< int > oldFrequencyTable(10, 1);
			std::vector< int > closedSet(4);
			finder.initializeFrequencytable(frequencyTable);
			std::vector<FrequencyValue> expected = {
				{ 1, 1, 0 }, { 1, 2, 1 }, { 2, 0, 2 },
				{ 2, 1, 1 }, { 2, 1, 2 }, { 2, 3, 1 },
				{ 3, 0, 0 }, { 3, 3, 0 },
				{ 4, 2, 0 } };

			std::vector<FrequencyValue> orderedFrequencies;
			finder.orderFrequencyTableInAscendingOrderAndZeroesDown(frequencyTable, oldFrequencyTable, std::numeric_limits<int>::max(), orderedFrequencies, closedSet);
			compareFrequencyValues(expected, orderedFrequencies);
		}

		TEST_METHOD(orderFrequencyValuesExcludeFoundColumns)
		{
			ECFinder finder = initECFinder();
			std::vector< int > frequencyTable(10);
			std::vector< int > oldFrequencyTable(10, 1);
			finder.initializeFrequencytable(frequencyTable);
			std::vector<FrequencyValue> expected = {
				{ 1, 1, 0 },
				{ 2, 1, 1 }, { 2, 1, 2 }, { 2, 3, 1 },
				{ 3, 3, 0 }};

			std::vector<FrequencyValue> orderedFrequencies;
			std::vector<int> closedSet = { 1, 0, 1, 0 };
			finder.orderFrequencyTableInAscendingOrderAndZeroesDown(frequencyTable, oldFrequencyTable, std::numeric_limits<int>::max(), orderedFrequencies, closedSet);
			compareFrequencyValues(expected, orderedFrequencies);
		}

		TEST_METHOD(copyZeroesToNewFrequencyTable)
		{
			ECFinder finder = initECFinder();
			std::vector< int > frequencyTable(10);
			std::vector< int > oldFrequencyTable(10, 1);
			std::vector< int > closedSet(4);
			finder.initializeFrequencytable(frequencyTable);
			oldFrequencyTable[0] = 0;
			std::vector<FrequencyValue> orderedFrequencies;
			finder.orderFrequencyTableInAscendingOrderAndZeroesDown(frequencyTable, oldFrequencyTable, std::numeric_limits<int>::max(), orderedFrequencies, closedSet);
			Assert::AreEqual(0, frequencyTable[0], L"Frequency zeros not copied");
		}

		TEST_METHOD(orderFrequencyValuesWithMaxLimit)
		{
			ECFinder finder = initECFinder();
			std::vector< int > frequencyTable(10);
			std::vector< int > oldFrequencyTable(10, 1);
			std::vector< int > closedSet(4);
			finder.initializeFrequencytable(frequencyTable);
			
			std::vector<FrequencyValue> expected = {
				{ 1, 1, 0 }, { 1, 2, 1 }, { 2, 0, 2 },
				{ 2, 1, 1 }, { 2, 1, 2 }, { 2, 3, 1 } };
			std::vector<FrequencyValue> orderedFrequencies;
			finder.orderFrequencyTableInAscendingOrderAndZeroesDown(frequencyTable, oldFrequencyTable, 3, orderedFrequencies, closedSet);
			compareFrequencyValues(expected, orderedFrequencies);
		}

		TEST_METHOD(filterRowsWithMinFrequency)
		{
			ECFinder finder = initECFinder();
			std::vector< int > newFrequencyTable(10);
			std::vector< int > table = {0,1,2,3,4
			};
			std::vector< int > expected = {2,4
			};
			std::vector< int > newSubMatrix( 2);
			finder.populateNewSubMatrixAndFrequencyTable(table, FrequencyValue{ 2, 0, 2 }, newSubMatrix, newFrequencyTable);
			compare(expected, newSubMatrix);
		}

		TEST_METHOD(calculateNewFrequencyTable)
		{
			ECFinder finder = initECFinder();
			std::vector< int > newSubMatrix(2);

			std::vector< int > oldSubmatrix = {0,1,2,3,4
			};
			std::vector< int> expected = {
				0, 0, 2,
				1, 1, 0,
				1, 1,
				2, 0 };
			std::vector< int > newFrequencyTable(expected.size());
			finder.populateNewSubMatrixAndFrequencyTable(oldSubmatrix, FrequencyValue{ 2, 0, 2 }, newSubMatrix, newFrequencyTable);
			compare(expected, newFrequencyTable);
		}

		TEST_METHOD(populateClosedSet)
		{
			ECFinder finder = initECFinder();
			std::vector< int> oldFrequencyTable = {
				3, 0, 2,
				1, 2, 2,
				4, 1,
				3, 2 };
			std::vector< int> newFrequencyTable = {
				0, 0, 2,
				1, 1, 0,
				1, 1,
				2, 0 };
			std::vector<int> expected = {
				3, 0, 0, 1
			};
			std::vector<int> closedSet(4);
			int maxFrequency = 2;
			bool isExisting = finder.populateClosedSetAndReturnIfExisting(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet);
			compare(expected, closedSet);
			Assert::IsFalse(isExisting, L"Closed set should be new");
		}

		TEST_METHOD(checkIfClosedSetIsExisting)
		{
			ECFinder finder = initECFinder();
			std::vector< int> oldFrequencyTable = {
				3, 0, 2,
				1, 2, 2,
				4, 1,
				0, 2 };
			std::vector< int> newFrequencyTable = {
				0, 0, 2,
				1, 1, 0,
				1, 1,
				2, 0 };
			std::vector<int> expected = {
				3, 0, 0, 1
			};
			std::vector<int> closedSet(4);
			int maxFrequency = 2;
			bool isExisting = finder.populateClosedSetAndReturnIfExisting(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet);
			Assert::IsTrue(isExisting, L"Closed set should be existing");
			compare(expected, closedSet);
		}

		TEST_METHOD(addNewClosedSet)
		{
			ECFinder finder = initECFinder();
			std::vector< int> oldFrequencyTable = {
				3, 0, 2,
				1, 2, 2,
				4, 1,
				3, 2 };
			std::vector< int> newFrequencyTable = {
				0, 0, 2,
				1, 1, 0,
				1, 1,
				2, 0 };

			std::vector<int> closedSet(4);
			int maxFrequency = 2;
			finder.currentGenerator[0] = 3;
			finder.addToResultAndReturnIfNew(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet);

			EquivalenceClassPointer result = finder.resultMap[{maxFrequency, closedSet}];
			Assert::AreEqual(maxFrequency, result->frequency, L"Equivalence classes frequencies do not match");
			compare(closedSet, result->closedSet);
			compare(finder.currentGenerator, result->generators);
		}

		TEST_METHOD(addExistingClosedSet)
		{
			ECFinder finder = initECFinder();
			std::vector< int> oldFrequencyTable = {
				3, 0, 2,
				1, 2, 2,
				4, 1,
				3, 2 };
			std::vector< int> newFrequencyTable = {
				0, 0, 2,
				1, 1, 0,
				1, 1,
				2, 0 };

			finder.currentGenerator = { 3, 0, 0, 0 };
			std::vector<int> closedSet1(finder.currentGenerator);
			int maxFrequency = 2;
			finder.addToResultAndReturnIfNew(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet1);
			oldFrequencyTable[2] = 0;
			finder.currentGenerator = { 0 ,0 , 0, 1 };
			std::vector<int> closedSet(finder.currentGenerator);
			bool isNew = finder.addToResultAndReturnIfNew(oldFrequencyTable, newFrequencyTable, maxFrequency, closedSet);
			Assert::IsFalse(isNew, L"Closed set should be existing");

			EquivalenceClassPointer result = finder.resultMap[{maxFrequency, closedSet}];
			Assert::AreEqual(maxFrequency, result->frequency, L"Equivalence classes frequencies do not match");
			compare(closedSet, result->closedSet);
			std::vector<int> subGenerator(result->generators.begin() + 4, result->generators.end());
			compare(finder.currentGenerator, subGenerator);
		}

		TEST_METHOD(findEquivalenceClass)
		{
			ECFinder finder = initECFinder();
			std::vector< int > oldSubmatrix = {0,1,2,3,4};
			std::vector<int> oldFrequencyTable(10, 1);
			std::vector<int> closedSet(4);
			FrequencyValue frequencyValue( 1, 1, 0 );
			finder.currentGenerator[frequencyValue.getOriginalTableCol()] = frequencyValue.getOriginalTableColValue();
			finder.findEquivalenceClass(oldSubmatrix, oldFrequencyTable, frequencyValue, closedSet);

			EquivalenceClassPointer result = finder.resultMap[{frequencyValue.frequency, { 3, 1, 2, 1 }}];
			compare({ 0, 1, 0, 0 }, result->generators);
		}

		TEST_METHOD(findEquivalenceClasses_checkExisting)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{1, { 3, 1, 2, 1 }}];
			std::vector<int> subGenerator(result->generators.begin() + 4, result->generators.end());
			compare({ 0, 0, 2, 0 }, subGenerator);
		}

		TEST_METHOD(findEquivalenceClasses_checkLowerLevel)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{1, { 3, 2, 1, 1 }}];
			std::vector<int> subGenerator(result->generators.begin(), result->generators.begin() + 4);
			compare({ 3, 2, 0, 0 }, subGenerator);
		}

		TEST_METHOD(findEquivalenceClasses_checkLowerLevelExisting)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{1, { 3, 2, 1, 1 }}];
			std::vector<int> subGenerator(result->generators.begin() + 4, result->generators.begin() + 8);
			compare({ 3, 0, 1, 0 }, subGenerator);
		}

		TEST_METHOD(findEquivalenceClasses_checkMiddleLevel)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{2, { 3, 0, 0, 1 }}];
			std::vector<int> subGenerator(result->generators.begin(), result->generators.begin() + 4);
			compare({ 3, 0, 0, 0 }, subGenerator);
		}

		TEST_METHOD(findEquivalenceClasses_checkDifferentSubBranchExisting)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{1, { 3, 2, 1, 1 }}];
			std::vector<int> subGenerator(result->generators.begin() + 8, result->generators.begin() + 12);
			compare({ 0, 2, 0, 1 }, subGenerator);
		}

		TEST_METHOD(findEquivalenceClasses_checkRowsAdded)
		{
			ECFinder finder = initECFinder();
			finder.findAllEquivalenceClasses();

			EquivalenceClassPointer result = finder.resultMap[{2, { 3, 0, 0, 1 }}];
			compare({ 2, 4 }, result->rows);
		}

// 		TEST_METHOD(findEquivalenceClasses_EquivalenceClassesAddedToRows)
// 		{
// 			ECFinder finder = initECFinder();
// 			finder.findAllEquivalenceClasses();
// 
// 			Assert::AreEqual(5, finder.rows[0], L"Not all Closed sets are connected to row 1");
// 		}

	private:
		void compare(std::vector< int > expected, std::vector< int > actual)
		{
			Assert::AreEqual(expected.size(), actual.size(), L"Row size mismatch");
			for (size_t i = 0; i < expected.size(); i++)
			{
				Assert::AreEqual(expected[i], actual[i], L"Does not match at");
			}
		}

		void compareFrequencyValues(std::vector< FrequencyValue > expected, std::vector< FrequencyValue > actual)
		{
			Assert::AreEqual(expected.size(), actual.size(), L"Row size mismatch");
			for (size_t i = 0; i < expected.size(); i++)
			{
				Assert::AreEqual(expected[i].frequency, actual[i].frequency, L"Frequency does not match at");
				Assert::AreEqual(expected[i].row, actual[i].row, L"Row does not match at");
				Assert::AreEqual(expected[i].col, actual[i].col, L"Col does not match at");
			}
		}
		ECFinder initECFinder()
		{
			std::vector<int> table = {
				1, 3, 1, 2,
				1, 3, 1, 1,
				3, 1, 2, 1,
				1, 2, 1, 2,
				3, 2, 1, 1
			};
			std::vector< size_t > colLength = { 0, 3, 6, 8, 10 };
			ECFinder finder(table, colLength, 1);
			return finder;
		}
	};
}
