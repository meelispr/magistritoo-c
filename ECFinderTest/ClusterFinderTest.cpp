#include "stdafx.h"
#include "ClusterFinder.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ECFinderTest
{
	TEST_CLASS(ClusterFinderTest)
	{
	public:
		
		TEST_METHOD(findDiscriminativenessIndex)
		{
			EquivalenceClassPointer candidateCluster = initCandidateCluster();
			initClusterFinder().findDiscriminativenessIndex(candidateCluster);

			Assert::AreEqual(2.25, candidateCluster->discriminativenessIndex, L"Discriminative index invalid");
		}
		
		TEST_METHOD(findHomogeneityIndex)
		{
			ClusterFinder clusterFinder = initClusterFinder();
			EquivalenceClassPointer candidateCluster = initCandidateCluster();
			
			clusterFinder.findDiscriminativenessIndex(candidateCluster);
			clusterFinder.findQualityIndex(candidateCluster);
			Assert::IsTrue(compare(0.5, candidateCluster->qualityIndex/candidateCluster->discriminativenessIndex, 0.001), L"Homogeneity index invalid");
		}
		
		TEST_METHOD(findQualityIndex)
		{
			ClusterFinder clusterFinder = initClusterFinder();
			EquivalenceClassPointer candidateCluster = initCandidateCluster();

			clusterFinder.findDiscriminativenessIndex(candidateCluster);
			clusterFinder.findQualityIndex(candidateCluster);
			Assert::IsTrue(compare(1.125, candidateCluster->qualityIndex, 0.001), L"Homogeneity index invalid");
		}
		
		/*TEST_METHOD(findFinalCluster)
		{
			std::vector<EquivalenceClassPointer> candidateClusters;

			EquivalenceClassPointer candidateCluster1 = initCandidateCluster();
			candidateClusters.push_back(candidateCluster1);

			EquivalenceClassPointer candidateCluster2 = initCandidateCluster();
			candidateClusters.push_back(candidateCluster2);

			EquivalenceClassPointer candidateCluster = initCandidateCluster();
			candidateCluster->generators = { 3, 0, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0 };
			candidateClusters.push_back(candidateCluster);

			EquivalenceClassPointer result = initClusterFinder().findFinalCluster(candidateClusters);
			Assert::AreEqual(candidateCluster->generators.size(), result->generators.size(), L"Max cluster invalid");
		}*/
		
// 		TEST_METHOD(reduceFrequenciesWithFinalCluster)
// 		{
// 			std::vector<EquivalenceClassPointer> candidateClusters;
// 			ClusterFinder clusterFinder = initClusterFinder(candidateClusters);
// 			
// 			EquivalenceClassPointer finalCluster = clusterFinder.findFinalCluster(candidateClusters);
// 			clusterFinder.removeExtractedRowsAndReduceFrequencies(finalCluster, candidateClusters);
// 			Assert::AreEqual(1, candidateClusters[0]->frequency, L"Max cluster invalid");
// 		}
		
		/*TEST_METHOD(removeExtractedRowsWithFinalCluster)
		{
			std::vector<EquivalenceClassPointer> candidateClusters;
			ClusterFinder clusterFinder = initClusterFinder(candidateClusters);

			EquivalenceClassPointer finalCluster = clusterFinder.findFinalCluster(candidateClusters);
			clusterFinder.removeExtractedRowsAndReduceFrequencies(finalCluster, candidateClusters);
			Assert::IsTrue(clusterFinder.rows[3] == 0, L"Max cluster invalid");
		}*/
		/*
		TEST_METHOD(findNextFinalCluster)
		{
			std::vector<EquivalenceClassPointer> candidateClusters;
			ClusterFinder clusterFinder = initClusterFinder(candidateClusters);

			EquivalenceClassPointer finalCluster = clusterFinder.findFinalCluster(candidateClusters);
			clusterFinder.removeExtractedRowsAndReduceFrequencies(finalCluster, candidateClusters);
			EquivalenceClassPointer nextFinalCluster = clusterFinder.findNextFinalCluster(candidateClusters);
			compare({0, 1}, nextFinalCluster->rows);
		}
		
		TEST_METHOD(reduceNextFinalClusterRows)
		{
			std::vector<EquivalenceClassPointer> candidateClusters;
			ClusterFinder clusterFinder = initClusterFinder(candidateClusters);

			EquivalenceClassPointer finalCluster = clusterFinder.findFinalCluster(candidateClusters);
			clusterFinder.removeExtractedRowsAndReduceFrequencies(finalCluster, candidateClusters);
			EquivalenceClassPointer nextFinalCluster = clusterFinder.findNextFinalCluster(candidateClusters);
			clusterFinder.removeExtractedRowsAndReduceFrequencies(nextFinalCluster, candidateClusters);
			EquivalenceClassPointer nextFinalCluster2 = clusterFinder.findNextFinalCluster(candidateClusters);
			clusterFinder.removeExtractedRowsAndReduceFrequencies(nextFinalCluster2, candidateClusters);
			compare({ 2 }, nextFinalCluster2->rows);
		}*/
		/*
		TEST_METHOD(findClusters)
		{
			std::vector<EquivalenceClassPointer> candidateClusters;
			ClusterFinder clusterFinder = initClusterFinder(candidateClusters);

			clusterFinder.findClusters(candidateClusters);

			compare({ 2 }, clusterFinder.result[2]->rows);
		}*/
	private:
		bool compare(double d1, double d2, double epsilon) {
			return fabs(d1 - d2) <= epsilon;
		}

		EquivalenceClassPointer initCandidateCluster()
		{
			std::vector<int> closedSet = { 3, 0, 1, 0 };
			std::vector<int> generators = { 3, 0, 0, 0, 0, 0, 1, 0 };
			return EquivalenceClassPointer(new EquivalenceClass(4, closedSet, generators));
		}

		ClusterFinder initClusterFinder()
		{
			//std::vector<std::vector<EquivalenceClassPointer>> rows;
			std::vector<int> table;
			return{ 4, 1, table };
		}

		ClusterFinder initClusterFinder(std::vector<EquivalenceClassPointer> &candidateClusters)
		{
			EquivalenceClassPointer candidateCluster1 = initCandidateCluster();
			candidateCluster1->frequency = 2;
			candidateCluster1->rows = { 2, 3 };
			candidateClusters.push_back(candidateCluster1);

			EquivalenceClassPointer candidateCluster2 = initCandidateCluster();
			candidateCluster2->frequency = 2;
			candidateCluster2->rows = { 0, 1 };
			candidateClusters.push_back(candidateCluster2);

			EquivalenceClassPointer candidateCluster3 = initCandidateCluster();
			candidateCluster3->frequency = 3;
			candidateCluster3->rows = { 3, 4, 5 };
			candidateClusters.push_back(candidateCluster3);

// 			std::vector<std::vector<EquivalenceClassPointer>> rows = {
// 				{ candidateCluster2 }, { candidateCluster2 },
// 				{ candidateCluster1 }, { candidateCluster1, candidateCluster3 },
// 				{ candidateCluster3 }, { candidateCluster3 } };
			std::vector<int> table;

			return { 4, 1, table };
		}

		void compare(std::vector< int > expected, std::vector< int > actual)
		{
			Assert::AreEqual(expected.size(), actual.size(), L"Row size mismatch");
			for (size_t i = 0; i < expected.size(); i++)
			{
				Assert::AreEqual(expected[i], actual[i], L"Does not match at");
			}
		}
	};
}